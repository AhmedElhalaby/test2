@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($Users as $user)
                    <br>
                    <div class="card">
                        <div class="card-header">

                        </div>
                        <div class="card-body">
                            {{$user->name}}
                            <span class="float-right">
                                <a href="{{url('follow/'.$user->id)}}" class="btn @if($user->is_follow()) btn-danger @else btn-primary @endif">@if($user->is_follow()) UnFollow @else Follow @endif</a>
                            </span>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
