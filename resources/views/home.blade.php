@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Post</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{url('create/post')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="content"></label><textarea required name="text" id="content"
                                                                           class="form-control"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                               
                                <div class="col-md-3">
                                    <input type="submit" class="btn btn-primary" value="Post">
                                </div>
                                <div class="col-md-9">
                                    <input type="file" class="form-control" name="image[]" multiple>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @foreach($Posts as $post)
                    <br>
                    <div class="card">
                        <div class="card-header">{{$post->user->name}}
                            <span class="float-right">
                                    <div class="dropdown show">
                                      <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                                         aria-haspopup="true" aria-expanded="false">
                                      </a>

                                      <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                          @if($post->user_id == \Illuminate\Support\Facades\Auth::user()->id)
                                              <a class="dropdown-item" href="#" data-toggle="modal" data-target="#post{{$post->id}}">Edit</a>
                                              <a class="dropdown-item" href="{{url('delete/post/'.$post->id)}}">delete</a>
                                          @else
                                              <a class="dropdown-item" href="{{url('share/post/'.$post->id)}}">share</a>
                                          @endif
                                      </div>
                                    </div>
                            </span>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @foreach($post->attachments as $attachment)
                                    <img src="{{asset($attachment->image)}}" alt="" width="100%" height="200px">
                                @endforeach
                            </div>
                            {{$post->content}}
                        </div>
                        <div class="card-footer">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="{{url('like/post/'.$post->id)}}" class="btn @if($post->is_like()) btn-success @else btn-primary @endif" style="margin-top: 23px;"><i class="fa  @if($post->is_like()) fa-thumbs-up @else fa-thumbs-o-up @endif"></i></a>
                            </div>
                            <div class="col-md-11">
                                <form action="{{url('comment/post/'.$post->id)}}" method="post">
                                    @csrf
                                    <label for="comment"></label><input type="text" class="form-control" id="comment" placeholder="add comment" name="comment">
                                </form>
                            </div>
                        </div>
                 
                            @foreach($post->comments as $commment)
                                <hr>
                                <p>{{$commment->user->name.' : '.$commment->comment}}</p>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal fade" id="post{{$post->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <form action="{{url('update/post/'.$post->id)}}" method="post" class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="content"></label>
                                            <textarea required name="text" id="content"
                                                      class="form-control">{{$post->content}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
