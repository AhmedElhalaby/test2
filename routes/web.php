<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@welcome');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('search', 'HomeController@search');

Route::get('follow/{id}','HomeController@follow_toggle');
Route::post('create/post','HomeController@create_post');
Route::get('delete/post/{id}','HomeController@delete_post');
Route::post('update/post/{id}','HomeController@update_post');
Route::post('comment/post/{id}','HomeController@comment_post');
Route::get('share/post/{id}','HomeController@share_post');
Route::get('like/post/{id}','HomeController@like_toggle');
