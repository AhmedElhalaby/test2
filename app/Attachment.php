<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $table = 'attachments';
    protected $fillable = ['post_id','image'];

    public function post(){
        return $this->belongsTo('App\Post','post_id','id');
    }
}
