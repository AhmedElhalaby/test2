<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $table = 'follows';
    protected $fillable = ['user_id','object_id'];


    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function object(){
        return $this->belongsTo('App\User','object_id','id');
    }
}
