<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Comment;
use App\Follow;
use App\Like;
use App\Post;
use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $user=Auth::user();
        $follow = Follow::where('user_id',$user->id)->pluck('object_id');
        $Posts = Post::whereIn('user_id',$follow)->orWhere('user_id',$user->id)->get();
        return view('home',compact('Posts'));
    }
    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function search(Request $request)
    {
        $Users = User::where('name','LIKE','%'.$request->search.'%')->where('id','!=',Auth::user()->id)->get();
        return view('search',compact('Users'));
    }
    public function follow_toggle($id)
    {
        $Follow = Follow::where('user_id',Auth::user()->id)->where('object_id',$id)->first();
        if($Follow){
            $Follow->delete();
        }else{
            $Follow = new Follow();
            $Follow->user_id = Auth::user()->id;
            $Follow->object_id = $id;
            $Follow->save();
        }
        return redirect()->back();
    }
    public function like_toggle($id)
    {
        $Like = Like::where('user_id',Auth::user()->id)->where('post_id',$id)->first();
        if($Like){
            $Like->delete();
        }else{
            $Like = new Like();
            $Like->user_id = Auth::user()->id;
            $Like->post_id = $id;
            $Like->save();
        }
        return redirect()->back();
    }
    public function welcome()
    {
        return view('welcome');
    }

    public function create_post(Request $request)
    {
        $request->validate([
            'text' => 'required|max:255',
        ]);
        $user=Auth::user();

        $post = new Post();
        $post->content = $request->text;
        $post->user_id = $user->id;
        $post->save();


        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile('image') ) {
            // 1. Generate a new file name
            foreach ($request->image as $file) {
                $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                // 2. Move the new file to the correct path
                $file_path = $file->move('public/posts/', $new_file_name);
                // 3. Save the complete path to the database
                $image= 'public/posts/'.$new_file_name;
                $attachment = new Attachment();
                $attachment->post_id = $post->id;
                $attachment->image = $image;
                $attachment->save();
            }

        }

        return redirect()->back();






    }
    public function delete_post($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect()->back();

    }
    public function update_post(Request $request,$id)
    {

        $request->validate([
            'text' => 'required|max:255',
        ]);
        $user=Auth::user();
        $post = Post::find($id);
        $post->content = $request->text;
        $post->save();

        return redirect()->back();

    }
    public function comment_post(Request $request,$id)
    {

        $request->validate([
            'comment' => 'required|max:255',
        ]);
        $user=Auth::user();
        $post = Post::find($id);
        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->post_id = $post->id;
        $comment->user_id = $user->id;
        $comment->save();
        return redirect()->back();

    }
    public function share_post($id)
    {

        $user=Auth::user();
        $post = Post::find($id);
        $new = new Post();
        $new->content = $post->content;
        $new->parent_id = $post->id;
        $new->user_id = $user->id;
        $new->save();
        return redirect()->back();

    }
}
