<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{

    protected $table = 'posts';
    protected $fillable = ['user_id','content','parent_id'];

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function parent(){
        return $this->belongsTo('App\Post','parent_id','id');
    }

    public function comments(){
        return $this->hasMany('App\Comment','post_id','id');
    }
    public function attachments(){
        return $this->hasMany('App\Attachment','post_id','id');
    }
    public function is_like(){
        $like = Like::where('user_id',Auth::user()->id)->where('post_id',$this->id)->first();
        if($like)
            return true;
        else
            return false;
    }
}
